from math import *
from os import path
import re
from time import time
from lib.links.channel import Channel

class Sequence:
    def __init__(self, conf):
        self.id = conf['id']
        self._channels = {}
        self._prefix = conf['prefix']
        enabled = conf['enabled'] if ('enabled' in conf) else True
        enabled_channel_conf = {
            "name": "enabled",
            "value": enabled,
            "datainfo": {
                "type": "bool"
            }
        }
        self.create_channel(enabled_channel_conf)
        status_channel_conf = {
            "name": "status",
            "value": [0, ''],
            "readonly": True,
            "datainfo": {
                "type": "tuple",
                "members": [
                    {
                        "type": "int"
                    },
                    {
                        "type": "string"
                    }
                ]
            }
        }
        self.create_channel(status_channel_conf)

    def create_channel(self, channel_conf):
        channel_name = re.sub(r'[^a-zA-Z0-9\.]', '_', channel_conf['name'])
        channel_id = f"{self.id}.{channel_name}"
        channel_conf['id'] = re.sub(r'[^a-zA-Z0-9\.]', '_', channel_id)
        if not channel_conf.get('readonly'):
            if not ('set topic' in channel_conf):
                channel_conf['set topic'] = path.join(self._prefix, channel_name, 'set')
        if not ('value topic' in channel_conf):
            channel_conf['value topic'] = path.join(self._prefix, channel_name)
        channel = Channel(channel_conf)
        self._channels[channel.name] = channel
        return channel

    def enable(self):
        return self._channels['enabled'].set(True)

    def disable(self):
        return self._channels['enabled'].set(False)

    def set_status(self, code, msg):
        self._channels['status'].set([code, msg])

    @property
    def enabled(self):
        return self._channels['enabled'].value

    @property
    def channels(self):
        return self._channels

    def serialize(self):
        structure = {
            'name': self.id,
            'type': self.__class__.__name__.lower(),
            'output': self.output.serialize(),
            'inputs': []
        }
        del structure['output']['name']
        for inp_id, inp in self.inputs.items():
            structure['inputs'].append(inp.serialize())
        return structure



class Expression(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        self._expression = conf['expression']
        self._global_env = {
            sin: sin,
            cos: cos,
            log: log,
            str: str,
            int: int,
            float: float
        }
        channels_conf = conf['channels']
        # Create output channel
        channel_conf = channels_conf['out'] if ('out' in channels_conf) else {}
        channel_conf['name'] = 'out'
        channel_conf['readonly'] = True
        self.create_channel(channel_conf)
        self._inputs = {}
        # Create input channels
        for channel_name, channel_conf in channels_conf.items():
            channel_conf['name'] = channel_name
            channel = self.create_channel(channel_conf)
            self._inputs[channel_name] = channel

    def tick(self, tick_count):
        if not self.enabled:
            self._channels['out'].valid = False
            return False
        for channel in self._inputs.values():
            if not channel.valid or not channel.has_new_input:
                return False
        # All inputs have new unhandled values, we're ready to calculate
        env = {}
        for channel_name, channel in self._inputs.items():
            env[channel_name] = channel.value
        result = eval(self._expression, self._global_env, env)
        self._channels['out'].set(result)
        for channel in self._inputs.values():
            channel.handled()
        return True

    def serialize(self):
        structure = super().serialize()
        structure['expression'] = self.expression
        return structure




class Splitter(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        channels_conf = conf['channels']
        # Create input channel
        channel_conf = channels_conf['in'] if ('in' in channels_conf) else {}
        channel_conf['name'] = 'in'
        channel_conf['datainfo'] = {
            "type": "string"
        }
        self.create_channel(channel_conf)
        # Create output channel
        channel_conf = channels_conf['out'] if ('out' in channels_conf) else {}
        channel_conf['name'] = 'out'
        channel_conf['readonly'] = True
        channel_conf['datainfo'] = {
            "type": "array",
            "members": {
                "type": "string"
            }
        }
        self.create_channel(channel_conf)
        # Set split char
        self._split_char = conf.get('split char')

    def tick(self, tick_count):
        if not self.enabled:
            self._channels['out'].valid = False
            return False
        if not self._channels['in'].valid or not self._channels['in'].has_new_input:
            return False
        # Input has new unhandled values, we're ready to calculate
        new_val = self._channels['in'].value.decode('UTF8')
        result = new_value.split(self._split_char)
        self._channels['out'].set(result)
        self._channels['in'].handled()
        return True

    def serialize(self):
        structure = super().serialize()
        structure['split char'] = self.split_char
        return structure



# FIXME: Not updated for new code/config structure
class ArraySelect(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        if len(self.inputs) != 1:
            raise TypeError(f"{self.__class__.__name__} '{self.id}' must have exactly 1 input. Got {len(self.inputs)}.")
        self.index = conf['index']

    def tick(self, tick_count):
        if not self.enabled:
            return
        inp = list(self.inputs.values())[0]
        if inp.handled:
            return False
        # All inputs have new unhandled values, we're ready to calculate
        if not isinstance(inp.value, list):
            raise TypeError(f"{self.__class__.__name__} '{self.id}' requires array (list) inputs. Got {inp.value.__class__.__name__}.")
        if self.index < len(inp.value):
             self.output.set(inp.value[self.index])
        else:
            self.output.set(None)
        inp.handled = True
        return True

    def serialize(self):
        structure = super().serialize()
        structure['index'] = self.index
        return structure


# FIXME: Not updated for new code/config structure
class HashSelect(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        if len(self.inputs) != 1:
            raise TypeError(f"{self.__class__.__name__} '{self.id}' must have exactly 1 input. Got {len(self.inputs)}.")
        self.key = conf['key']

    def tick(self, tick_count):
        if not self.enabled:
            return
        inp = list(self.inputs.values())[0]
        if inp.handled:
            return False
        # All inputs have new unhandled values, we're ready to calculate
        if not isinstance(inp.value, dict):
            raise TypeError(f"{self.__class__.__name__} '{self.id}' requires array (list) inputs. Got {inp.value.__class__.__name__}.")
        if self.key in inp.value:
             self.output.set(inp.value[self.key])
        else:
            self.output.set(None)
        inp.handled = True
        return True

    def serialize(self):
        structure = super().serialize()
        structure['key'] = self.key
        return structure



class Regex(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        channels_conf = conf['channels']
        # Create input channel
        channel_conf = channels_conf['in'] if ('in' in channels_conf) else {}
        channel_conf['name'] = 'in'
        channel_conf['datainfo'] = {
            "type": "string"
        }
        self.create_channel(channel_conf)
        # Create output channel
        channel_conf = channels_conf['out'] if ('out' in channels_conf) else {}
        channel_conf['name'] = 'out'
        channel_conf['readonly'] = True
        channel_conf['datainfo'] = {
            "type": "array",
            "members": {
                "type": "string"
            }
        }
        self.create_channel(channel_conf)
        # Setup regex
        self.regex = conf['regex']
        self.c_regex = re.compile(self.regex)

    def tick(self, tick_count):
        if not self.enabled:
            self._channels['out'].valid = False
            return False
        if not self._channels['in'].valid or not self._channels['in'].has_new_input:
            return False
        # Input has new unhandled values, we're ready to calculate
        new_val = self._channels['in'].value.decode('UTF8')
        res = self.c_regex.search(new_val)
        if res:
            self._channels['out'].set(res.groups())
        else:
            self._channels['out'].set([])
        self._channels['in'].handled()
        return True

    def serialize(self):
        structure = super().serialize()
        structure['regex'] = self.regex
        return structure

# TODO: Add automatic calculation of min/max values
class UnitConverter(Sequence):
    converters = {
        # Temperature
        "K": {
            "C": lambda k: k - 273.15,
            "F": lambda k: (k * 9 / 5) - 459.67
        },
        "C": {
            "K": lambda c: c + 273.15,
            "C": lambda c: (c * 9 / 5) + 32
        },
        "F": {
            "K": lambda f: (f + 459.67) * 5 / 9,
            "C": lambda f: (f - 32) * 5 / 9
        },
        # Pressure
        "bar": {
            "mbar": lambda b: (b * 1000),
            "Pa": lambda b: (b * 100000),
            "torr": lambda b: (b * 750.0616827),
            "psi": lambda b: (b * 14.503773773)
        },
        "mbar": {
            "bar": lambda b: (b * 0.001),
            "Pa": lambda b: (b * 100),
            "torr": lambda b: (b * 0.7500616827),
            "psi": lambda b: (b * 0.0145037738)
        },
        "Pa": {
            "bar": lambda p: (p * 0.00001),
            "mbar": lambda p: (p * 0.01),
            "torr": lambda p: (p * 0.0075006168),
            "psi": lambda p: (p * 0.0001450377)
        },
        "torr": {
            "bar": lambda t: (t * 0.0013332237),
            "mbar": lambda t: (t * 1.3332236842),
            "Pa": lambda t: (t * 133.3223684211),
            "psi": lambda t: (t * 0.0193367747)
        },
        "psi": {
            "bar": lambda p: (p * 0.0689475729),
            "mbar": lambda p: (p * 68.947572932),
            "Pa": lambda p: (p * 6894.7572932),
            "torr": lambda p: (p * 51.714932572)
        }
    }

    def __init__(self, conf):
        super().__init__(conf)
        channels_conf = conf['channels']
        # Create input channel
        channel_conf = channels_conf['in'] if ('in' in channels_conf) else {}
        channel_conf['name'] = 'in'
        channel_conf['datainfo'] = {
            "type": "double"
        }
        self.create_channel(channel_conf)
        # Create output channel
        channel_conf = channels_conf['out'] if ('out' in channels_conf) else {}
        channel_conf['name'] = 'out'
        channel_conf['datainfo'] = {
            "type": "double"
        }
        self.create_channel(channel_conf)
        fr = conf['from']
        if not fr in self.converters:
            raise AttributeError(f"Unsupported/unknown source unit: {fr}")
        to = conf['to']
        if not to in self.converters[fr]:
            raise AttributeError(f"Unsupported/unknown destination unit: {to}")
        self._converter = self.converters[fr][to]
        self._reverse = self.converters[to][fr]

    def tick(self, tick_count):
        if not self.enabled:
            self._channels['out'].valid = False
            return False
        if self._channels['in'].valid and self._channels['in'].has_new_input:
            # New input value from source
            new_val = self._channels['in'].value
            out_val = self._converter(new_val)
            self._channels['out'].set(out_val)
            self._channels['out'].handled()
            self._channels['in'].handled()
            return True
        if self._channels['out'].valid and self._channels['out'].has_new_input:
            # New input value from source
            new_val = self._channels['out'].value
            reverse_val = self._reverse(new_val)
            self._channels['in'].set(reverse_val)
            self._channels['in'].handled()
            self._channels['out'].handled()
            return True
        return False



class MovingAverage(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        channels_conf = conf['channels']
        # Create input channel
        channel_conf = channels_conf['in'] if ('in' in channels_conf) else {}
        channel_conf['name'] = 'in'
        channel_conf['datainfo'] = {
            "type": "double"
        }
        self.create_channel(channel_conf)
        # Create output channel
        channel_conf = channels_conf['out'] if ('out' in channels_conf) else {}
        channel_conf['name'] = 'out'
        channel_conf['readonly'] = True
        channel_conf['datainfo'] = {
            "type": "double"
        }
        self.create_channel(channel_conf)
        self._window = int(conf['window']) if ('window' in conf) else 10
        self._buffer = []
        self._avg = 0.0

    def tick(self, tick_count):
        if not self.enabled:
            self._channels['out'].valid = False
            return False
        if not self._channels['in'].valid or not self._channels['in'].has_new_input:
            return False
        # New input value available
        new_val = self._channels['in'].value
        self._buffer.append(new_val)
        if len(self._buffer) == 1:
            self._avg = new_val
            return False
        if len(self._buffer) < self._window:
            old_val = self._buffer[0]
        else:
            old_val = self._buffer.pop(0)
        self._avg = self._avg + ((new_val - old_val) / len(self._buffer))
        self._channels['out'].set(self._avg)
        self._channels['in'].handled()
        return True


class WeightedMovingAverage(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        channels_conf = conf['channels']
        # Create input channel
        channel_conf = channels_conf['in'] if ('in' in channels_conf) else {}
        channel_conf['name'] = 'in'
        channel_conf['datainfo'] = {
            "type": "double"
        }
        self.create_channel(channel_conf)
        # Create output channel
        channel_conf = channels_conf['out'] if ('out' in channels_conf) else {}
        channel_conf['name'] = 'out'
        channel_conf['readonly'] = True
        channel_conf['datainfo'] = {
            "type": "double"
        }
        self.create_channel(channel_conf)
        self._window = int(conf['window']) if ('window' in conf) else 10
        self._buffer = []
        self._avg = 0.0

    def tick(self, tick_count):
        if not self.enabled:
            self._channels['out'].valid = False
            return False
        if not self._channels['in'].valid or not self._channels['in'].has_new_input:
            return False
        # New input value available
        new_val = self._channels['in'].value
        self._buffer.insert(0, new_val)
        if len(self._buffer) <= self._window:
            old_val = self._buffer[-1]
        else:
            old_val = self._buffer.pop()
        total = 0
        denom = 0
        for i, val in enumerate(self._buffer):
            total += val / (i + 1)
            denom += 1 / (i + 1)
        self._avg = total / denom
        self._channels['out'].set(self._avg)
        self._channels['in'].handled()
        return True



class LUT(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        channels_conf = conf['channels'] if ('channels' in conf) else {}
        # Create output channel
        channel_conf = channels_conf['in'] if ('in' in channels_conf) else {}
        channel_conf['name'] = 'in'
        channel_conf['readonly'] = False
        self.create_channel(channel_conf)
        # Create output channels
        self._outputs = {}
        for channel_name, channel_conf in channels_conf.items():
            channel_conf['name'] = channel_name
            channel = self.create_channel(channel_conf)
            self._outputs[channel.name] = channel
        self._tick_divisor = int(conf['tick divisor'])
        # Get table
        table = conf['table']
        keys = []
        rows = {}
        for row in table:
            key = row['in']
            keys.append(key)
            del(row['in'])
            rows[key] = row
        keys.sort(reverse = True)
        self._table = {}
        for key in keys:
            self._table[key] = rows[key]
        print(self._table)

    def tick(self, tick_count):
        if not self.enabled:
            return False
        if tick_count % self._tick_divisor == 0:
            ready = self.update_outputs()
            return True
        if self._channels['in'].valid and self._channels['in'].has_new_input:
            self.calculate_outputs()
            self._channels['in'].handled()

    def calculate_outputs(self):
        in_val = float(self._channels['in'].value)
        for key, row in self._table.items():
            if in_val >= key:
                for channel_name, val in row.items():
                    self._channels[channel_name].set(val)
                break

    def update_outputs(self):
        for channel in self._outputs.values():
            channel.has_new_output = True


class DeadBandRegulator(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        channel_names = [
            'setpoint',
            'in',
            'deadband',
            'stepsize',
            'delay',
            'out'
        ]
        channels_conf = conf['channels'] if ('channels' in conf) else {}
        for channel_name in channel_names:
            channel_conf = channels_conf[channel_name] if (channel_name in channels_conf) else {}
            channel_conf['name'] = channel_name
            datainfo = {}
            datainfo['type'] = 'double'
            datainfo['unit'] = channel_conf.get('unit')
            datainfo['min'] = channel_conf.get('min')
            datainfo['max'] = channel_conf.get('max')
            datainfo['clamp on validation'] = True
            channel_conf['datainfo'] = datainfo
            channel = self.create_channel(channel_conf)
        # Create stable channel
        stable_channel_conf = {
            "name": "stable",
            "value": False,
            "datainfo": {
                "type": "bool"
            }
        }
        self.create_channel(stable_channel_conf)
        # Get config values
        self._dt = conf['dt']
        self._tick_divisor = int(conf['tick divisor'])
        self._prefix = conf['prefix']
        self.reset()
        self.disable()

    def reset(self):
        self._channels['out'].sync_value()  # Make sure readback value is the same as the actual current value
        self._error = 0
        self._last_change = time() - self._channels['delay'].value - 1
        self._channels['stable'].set(False)

    def enable(self, set_status = True):
        if set_status:
            self.set_status(120, 'enabling')
        self.reset()
        return super().enable()

    def disable(self, set_status = True):
        if set_status:
            self.set_status(0, 'disabled')
        return super().disable()

    def tick(self, tick_count):
        if self._channels['setpoint'].has_new_input:
            self._channels['setpoint'].handled()
        if self._channels['enabled'].has_new_input:
            self._channels['enabled'].handled()
            if not self.enabled:
                self._channels['out'].valid = False
                self.set_status(0, 'disabled')
        if not self.enabled:
            return False
        if tick_count % self._tick_divisor == 0:
            ready = self.regulate()
            return ready

    def regulate(self):
        print(self)
        if not self._channels['in'].valid:
            self.set_status(130, 'waiting for in value')
            return
        if not self._channels['setpoint'].valid:
            self.set_status(130, 'waiting for setpoint value')
            return
        if self._last_change + self._channels['delay'].value > time():
            self.set_status(180, 'stabilizing')
            return
        self._error = self._channels['setpoint'].value - self._channels['in'].value
        deadband = self._channels['deadband'].value
        stepsize = self._channels['stepsize'].value
        out_val = self._channels['out'].readback_value
        if abs(self._error * 2) > deadband:
            self._last_change = time()
            self._channels['stable'].set(False)
            if self._error > 0:
                out_val += stepsize
            else:
                out_val -= stepsize
            self.set_status(180, 'stabilizing')
        else:
            self._channels['stable'].set(True)
            self.set_status(190, 'stable')
        self._channels['out'].set(out_val)

    def __str__(self):
        str = f"DeadBandRegulator " \
            f"[DeadBand={self._channels['deadband'].value}, " \
            f"stepsize={self._channels['stepsize'].value}, " \
            f"delay={self._channels['delay'].value}], " \
            f"[setpoint={self._channels['setpoint'].value}, " \
            f"in={self._channels['in'].value}, " \
            f"error={self._error}, " \
            f"out={self._channels['out'].value}, " \
            f"status={self._channels['status'].value}]"
        return str



class PID(Sequence):
    def __init__(self, conf):
        super().__init__(conf)
        channel_names = [
            'setpoint',
            'in',
            'Kp',
            'Ki',
            'Kd',
            'p',
            'i',
            'd',
            'out'
        ]
        channels_conf = conf['channels'] if ('channels' in conf) else {}
        for channel_name in channel_names:
            channel_conf = channels_conf[channel_name] if (channel_name in channels_conf) else {}
            channel_conf['name'] = channel_name
            datainfo = {}
            datainfo['type'] = 'double'
            datainfo['unit'] = channel_conf.get('unit')
            datainfo['min'] = channel_conf.get('min')
            datainfo['max'] = channel_conf.get('max')
            datainfo['clamp on validation'] = True
            channel_conf['datainfo'] = datainfo
            channel = self.create_channel(channel_conf)
        self._dt = conf['dt']
        self._tick_divisor = int(conf['tick divisor'])
        self._i_limit = conf['i limit']
        self._prefix = conf['prefix']
        self.reset()

    def set_error(self, err):
        self._error_msg = err

    @property
    def error(self):
        return self._error_msg

    def reset(self):
        self._e_prev = 0
        self._channels['p'].set(0)
        self._channels['i'].set(0)
        self._channels['d'].set(0)
        self._error_msg = None

    def tick(self, tick_count):
        if self._channels['setpoint'].has_new_input:
            self._channels['setpoint'].handled()
            self.reset()
        if not self.enabled:
            self._channels['out'].valid = False
            return False
        if tick_count % self._tick_divisor == 0:
            ready = self.regulate()
            print(self)
            return ready

    def regulate(self):
        if not self._channels['in'].valid or not self._channels['setpoint'].valid:
            return
        error = self._channels['setpoint'].value - self._channels['in'].value
        self._channels['p'].set(error)
        self._channels['i'].set(self._channels['i'].value + error * self._dt)
        self._channels['d'].set((error - self._e_prev) / self._dt)
        Kp = self._channels['Kp'].value
        Ki = self._channels['Ki'].value
        Kd = self._channels['Kd'].value
        p = self._channels['p'].value
        i = self._channels['i'].value
        d = self._channels['d'].value
        out_val = Kp * (p + Ki * i + Kd * d)
        self._e_prev = error
        # Check for integral windup
        #if i > self._i_limit or i < -self._i_limit:
        #    self.disable()
        #    self.set_error('Integral windup')
        #    return
        # Set output channel
        self._channels['out'].set(out_val)

    def __str__(self):
        str = f"PID [Kp={self._channels['Kp'].value}, " \
            f"Ki={self._channels['Ki'].value}, " \
            f"Kd={self._channels['Kd'].value}, " \
            f"iLim={self._i_limit}] " \
            f"[p={self._channels['p'].value}, " \
            f"i={self._channels['i'].value}, " \
            f"d={self._channels['d'].value}] " \
            f"[setpoint={self._channels['setpoint'].value}, " \
            f"in={self._channels['in'].value}, " \
            f"error={self._e_prev}, " \
            f"out={self._channels['out'].value}, " \
            f"msg={self._error_msg}]"
        return str

    def serialize(self):
        structure = super().serialize()
        structure['i_limit'] = self._i_limit
        return structure





sequence_classes = {
    'pid': PID,
    'dead band regulator': DeadBandRegulator,
    'expression': Expression,
    'splitter': Splitter,
    'arrayselect': ArraySelect,
    'hashselect': HashSelect,
    'regex': Regex,
    'unit converter': UnitConverter,
    'moving average': MovingAverage,
    'weighted moving average': WeightedMovingAverage,
    'lut': LUT
}


def SequenceFactory(seq_conf):
    seq_type = seq_conf['type'].lower()
    if not seq_type in sequence_classes:
        raise TypeError(f"Unrecognized sequence type: {seq_type}")
    seq_class = sequence_classes[seq_type]
    sequence = seq_class(seq_conf)
    return sequence

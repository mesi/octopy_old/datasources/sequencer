import sys
import json
import logging
from os import path
from sequence import *
from time import time, sleep
from lib.octopyapp.octopyapp import OctopyApp
import traceback

APP_ID = 'Sequencer'
LOG_LEVEL = logging.DEBUG

STORAGE_CATEGORY = 'sequences'

STATUS_PUBLISH_INTERVAL = 2
OUTPUT_PUBLISH_INTERVAL = 1

class Sequencer(OctopyApp):
    def __init__(self):
        super().__init__(APP_ID, LOG_LEVEL)
        self.id = 'sequencer'
        self.sequences = {}
        self.inputs = {}
        self.outputs = []
        self.channels = []


    def start(self):
        super().start(start_loop = False)
        self.subscribe(self.topics['sequencer']['add sequence'], self.on_set_sequence)
        self.load_sequences()
        self.main_loop()


    def main_loop(self):
        tick_size = 1 / self.config["sequencer"]["frequency"]
        last_tick_time = time()
        last_status_publish_time = time()
        last_output_publish_time = time()
        time_drift = 0
        self.tick_count = 0
        while True:
            try:
                self.mqtt_client.loop(tick_size / 10)
                self.mqtt_client.loop_misc()
                current_time = time()
                if current_time >= last_tick_time + tick_size - time_drift:
                    time_drift += (current_time - last_tick_time) - tick_size
                    last_tick_time = current_time
                    self.tick()
                if current_time > last_status_publish_time + STATUS_PUBLISH_INTERVAL:
                    # Publish datasource status and channels regularly
                    last_status_publish_time = current_time
                    self.publish_status()
                if current_time > last_output_publish_time + OUTPUT_PUBLISH_INTERVAL:
                    # Publish all outputs regardless of status
                    last_output_publish_time = current_time
                    self.publish_channels(force = True)
                else:
                    # Publish only outputs that have changed
                    self.publish_channels()
            except RuntimeError:
                self.halt('Runtime error')
            except KeyboardInterrupt:
                self.halt('Keyboard interrupt')
            except BaseException as err:
                logging.error(err)
                if LOG_LEVEL == logging.DEBUG:
                    traceback.print_exc()


    def halt(self, reason):
        logging.info('Exiting: ' + reason)
        # Publish empty status message with disconnected state
        self.publish_status(empty = True)
        # Handle any queued messages
        self.mqtt_client.loop()
        self.mqtt_client.loop()
        self.mqtt_client.loop()
        # Stop the MQTT loop and disconnect from the server
        self.stop()
        sys.exit(0)


    def tick(self):
        self.tick_count += 1
        for seq_id, sequence in self.sequences.items():
            if hasattr(sequence, "tick") and callable(sequence.tick):
                sequence.tick(self.tick_count)


    def on_set_channel_message(self, client, userdata, msg):
        # New input value
        if msg.topic in self.inputs:
            self.update_input_value(msg)


    def update_input_value(self, msg):
        payload = msg.payload
        topic = msg.topic
        try:
            value = json.loads(payload)
            if isinstance(value, list) and (len(value) == 1 or (len(value) == 2 and isinstance(value[1], dict))):
                # Looks like SECoP-style data - extract just the value
                value = value[0]
        except json.decoder.JSONDecodeError as err:
            # Looks like it's not JSON data
            pass
        for inp in self.inputs[topic]:
            try:
                inp.set(value, from_source = True)
            except BaseException as e:
                self.log(logging.WARNING, f"Error updating input value '{inp.name}': {e}")


    def publish_channels(self, force = False):
        for output in self.outputs:
            if (output.has_new_output or force) and output.valid:
                payload = [
                    output.value,
                    { "t": time() }
                ]
                self.publish(output.value_topic, json.dumps(payload))
                output.written()


    def load_sequences(self):
        cmd = json.dumps({
            'category': STORAGE_CATEGORY,
            'key': self.config['system id'],
            'return topic': self.topics['sequencer']['load sequences']
        }, indent = 4)
        self.log(logging.INFO, f"Loading automation sequences")
        self.subscribe(self.topics['sequencer']['load sequences'], self.on_load_sequences)
        self.publish(topic = self.topics['storage']['get'], payload = cmd)


    def on_load_sequences(self, client, userdata, msg):
        try:
            payload = json.loads(msg.payload)
            if not payload:
                self.log(logging.ERROR, f"Error loading sequences. Storage Manager gave empty response.")
                return
            elif 'error' in payload:
                self.log(logging.ERROR, f"Error loading sequences. Storage Manager says: {payload['error']}.")
                return
            sequences_conf = payload['value']
            for seq_conf in sequences_conf:
                try:
                    sequence = SequenceFactory(seq_conf)
                except TypeError as err:
                    self.log(logging.ERROR, f"Error creating sequence: {err}")
                    traceback.print_exc()
                    continue
                self.log(logging.INFO, f"Sequence loaded: {sequence.id}")
                self.sequences[sequence.id] = sequence
            self.link_inputs()
            self.link_outputs()
            self.setup_channels()
        except json.decoder.JSONDecodeError as err:
            self.log(logging.ERROR, f"Error loading sequences due to JSON parse error: {err}")
        except Exception as err:
            self.log(logging.ERROR, f"Error loading sequences: {err}")
            traceback.print_exc()


    def on_set_sequence(self, client, userdata, msg):
        try:
            seq_conf = json.loads(msg.payload)
            seq_id = seq_conf['id']
            if seq_id in self.sequences:
                # Sequence already exists - update it
                self.log(logging.INFO, f"Updating sequence '{seq_id}'")
                sequence = self.sequences[seq_id]
                sequence.update(seq_conf)
            else:
                # New sequence
                self.log(logging.INFO, f"Creating new sequence '{seq_id}'")
                sequence = SequenceFactory(seq_conf)
                self.sequences[sequence.id] = sequence
            self.link_inputs()
        except json.decoder.JSONDecodeError as err:
            self.log(logging.ERROR, f"Error configuring sequences due to JSON parse error: {err}")
        except Exception as err:
            self.log(logging.WARNING, f"Could not configure sequence object: {err}")


    def link_inputs(self):
        self.log(logging.INFO, f"Channeling sequence inputs")
        # Unsubscribe to current list
        for topic in self.inputs:
            self.unsubscribe(topic)
        self.inputs.clear()
        # Gather all inputs
        for seq_id, sequence in self.sequences.items():
            for name, channel in sequence.channels.items():
                if channel.set_topic:
                    if not channel.set_topic in self.inputs:
                        self.inputs[channel.set_topic] = []
                    self.inputs[channel.set_topic].append(channel)
                    self.log(logging.DEBUG, f"Channel '{sequence.id}:{channel.name}' channeled to receive data on topic '{channel.set_topic}'")
        # Subscribe to all input topics
        for topic in self.inputs:
            self.subscribe(topic, self.on_set_channel_message)


    def lin_koutputs(self):
        self.outputs.clear()
        for seq_id, sequence in self.sequences.items():
            for name, channel in sequence.channels.items():
                if channel.value_topic:
                    self.outputs.append(channel)
                    self.log(logging.DEBUG, f"Channel '{sequence.id}:{channel.name}' channeled to publish data on topic '{channel.value_topic}'")


    def setup_channels(self):
        self.channels.clear()
        for seq_id, sequence in self.sequences.items():
            for name, channel in sequence.channels.items():
                channel.datasource_id = self.id
                self.channels.append(channel)


    def publish_status(self, empty = False):
        links = []
        if not empty:
            for channel in self.channels:
                links.append(channel.serialize())
            status = 'connected'
        else:
            status = 'disconnected'
        structure = {
            'links': links,
            'datasource id': self.id,
            'type': 'sequencer',
            'status': status
        }
        payload = json.dumps(structure, indent = 4)
        topic = path.join(self.topics['datasource']['status'], self.id)
        logging.debug(f"Publishing status for Sequencer datasource")
        self.publish(topic, payload)



if __name__ == '__main__':
    seq = Sequencer()
    seq.start()
